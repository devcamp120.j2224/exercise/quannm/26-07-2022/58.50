package com.devcamp.j5850.j5850;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J5850Application {

	public static void main(String[] args) {
		SpringApplication.run(J5850Application.class, args);
	}

}
